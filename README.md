# ⭐ Todo Application designed with Acceptance Test Driven Design Methodology
* Demo: http://35.225.31.207:8080 , add '/todos' to see the todo list
* The application is developed with NodeJS and ExpressJS as a Restfull api
* MongoDB used as a database for the backend (MongoDB atlas used for easy deployment)

# 🚀 Tech Stack
* NodeJS
* ExpressJS
* MongoDB
* mongodb-memory-server (for database mocking)

# 💿 How to run local
```
# Clone this repository
https://gitlab.com/hakki.ulku.developer/todo-backend.git
# Go into the repository
cd todo-backend
# Install the dependencies
$ npm install
# You need to modify example.env file with your own configs
# Start to see the application in the localhost:8080
$ npm run start
```

# 💿 How to test local
```
# Run all the tests and see the results
$ npm run test
```

# 🧠 Architectural Decisions
* Application has been running via index.js as a restful api
* For todo operations, there is a folder (todos) so that all api routes and services are developed under that (to have a modular structure for the readibility and maintainability)
* Todo Model is also under todos folder
* All tests (unit tests and integration tests) are developed under tests folder
* The contract is tested under provider-contract.js and reached the contract via Pact Flow
* All the development processes are driven by Test Drive Design Methodology

# ✈️ Deployment (Docker, Google Kubernetes)
* install gcloud cli
* login gcloud (you need an account for Google Cloud Platform)
* create gcloud project (created as React-Todo-Frontend in my case)
* set the project as default in your local: gcloud config set react-todo-frontend (in my case)
* Dockerize the application (check Dockerfile)
* build the docker image with the command: docker build -t node-todo .
* configure docker for gcloud: gcloud auth configure-docker
* tag the image: docker tag node-todo gcr.io/react-todo-frontend/node-todo
* push the docker image to Google Container Registry: docker push gcr.io/react-todo-frontend/node-todo
* create Google Kubernetes Engine cluster: gcloud container clusters create node-todo
* setup your node: kubectl create -f ./kubernetes/node-deployment.yaml (see "node-deployment.yaml" file)
* create a load balancer to access the API externally: kubectl create -f ./kubernetes/node-service.yaml (see the file "kubernetes/node-service.yaml")
* Then have the urls: kubectl get service node
* you should see EXTERNAL-API field and go to browser and see http://EXTERNAL_IP:8080/todos 
* it is done you have a running API on cloud :)








