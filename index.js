require('dotenv').config()
const express = require('express')
const todosRouter = require('./todos/todos.router')
const cors = require("cors")
const bodyParser = require('body-parser')
const mongoose = require('mongoose');

let app
mongoose.connect(process.env.MONGODB_URI).then(() => {
    app = express()
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json())
    app.use(bodyParser.raw());
    app.use(cors())

    app.use('/', todosRouter)

    app.listen(process.env.PORT, () => {
        console.log("Server is up and running on port: " + process.env.PORT);
    })
})

module.exports = {
    app
}