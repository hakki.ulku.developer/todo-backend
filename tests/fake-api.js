const express = require("express");
const todosRouter = require("../todos/todos.router")
const bodyParser = require("body-parser");
require('dotenv').config()

const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use(bodyParser.raw());

app.use("/", todosRouter)

// fake api for tests
module.exports = app