const { Verifier } = require('@pact-foundation/pact');
require('dotenv').config()
const db = require('./test-db');
const Todo = require('../todos/todo.model');
const { testTodos } = require('./test-data');
const { v4: uuidv4 } = require('uuid');

const port = 8080
const app = require('./fake-api')
const server = app.listen(port)

// Setup connection to the database
beforeAll(async () => { await db.connect() })
//beforeEach(async () => { await db.clear() })
afterAll(async () => { await db.close() })

jest.setTimeout(100000);

describe('Pact Verification', () => {

    beforeAll(async () => {
        // add some new todos to the mock database
        for (const todo of testTodos) {
            let newTodoToAdd = new Todo({ todoName: todo.todoName, id: uuidv4() })
            await newTodoToAdd.save()
        }
    })

    it('should validate the expectations of our consumer', () => {
        const opts = {
            provider: 'TodoAppBackend',
            providerBaseUrl: `${process.env.BACKEND_BASE_URL}:${port}`,
            pactUrls: ["https://hakkiulku.pactflow.io/pacts/provider/Todo%20App%20Backend/consumer/Todo%20App%20Frontend/version/1.0.7"],
            pactBrokerToken: process.env.PACT_BROKER_TOKEN,
            publishVerificationResult: true,
            providerVersion: '1.0.0',
            logLevel: 'INFO',
        };

        return new Verifier(opts).verifyProvider();
    });
});

afterAll(() => {
    server.close()
})