const app = require('./fake-api')
const request = require("supertest");
const db = require('./test-db');
const { v4: uuidv4 } = require('uuid');
const { testTodos } = require('./test-data');
const Todo = require('../todos/todo.model');

// Setup connection to the database
beforeAll(async () => { await db.connect() })
beforeEach(async () => { await db.clear() })
afterAll(async () => { await db.close() })

jest.setTimeout(100000);

describe('Todos route responds as expected', () => {
    it("GET: /todos should return an empty array or an array of todos", async () => {
        let response = await request(app).get('/todos')
        expect(Array.isArray(response.body)).toBe(true)

        // add some new todos to the mock database
        for (const todo of testTodos) {
            let newTodoToAdd = new Todo({ todoName: todo.todoName, id: uuidv4() })
            await newTodoToAdd.save()
        }

        let { body } = await request(app).get('/todos')

        // if first obj includes todoName & id keys then it's validated
        if (body.length !== 0) {
            expect(body[0]).toHaveProperty("id")
            expect(body[0]).toHaveProperty("todoName")
        }
    })

    it("POST: /addTodo should add a new todo to backend and return the added todo with 201 code", async () => {
        const uniqueStringName = uuidv4()
        const newTodo = { todoName: uniqueStringName }
        const response = await request(app)
            .post("/addTodo")
            .set('Content-type', 'application/json')
            .send(newTodo)

        expect(response.body.todoName).toEqual(newTodo.todoName)
        expect(response.statusCode).toBe(201)

        const todos = await Todo.find().select({ "todoName": 1, "id": 1, "_id": 0 })
        expect(todos.map(todo => todo.todoName).includes(uniqueStringName)).toBe(true)
        expect(todos.map(todo => todo.id).includes(response.body.id)).toBe(true)

    })
})
