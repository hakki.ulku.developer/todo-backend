const db = require('./test-db')
const { getAllTodo, addTodo } = require('../todos/todos.services')
const Todo = require('../todos/todo.model')
const { v4: uuidv4 } = require('uuid');
const { testTodos } = require('./test-data');

jest.setTimeout(100000)

// Setup connection to the database
beforeAll(async () => { await db.connect() })
beforeEach(async () => { await db.clear() })
afterAll(async () => { await db.close() })

// UNIT TESTS FOR THE TODOS SERVICE HELPERS
describe('Service functions run correctly', () => {

    it('getAllTodo function should return an empty array if there is no todo yet, otherwise an array of todos', async () => {

        // WHEN the db is empty
        const response = await getAllTodo()
        expect(Array.isArray(response)).toBe(true)

        // add some new todos to the mock database
        for (const todo of testTodos) {
            let newTodoToAdd = new Todo({ todoName: todo.todoName, id: uuidv4() })
            await newTodoToAdd.save()
        }

        // WHEN there are todos in the db
        const todosFromDB = await getAllTodo()
        expect(Array.isArray(todosFromDB)).toBe(true)
        expect(todosFromDB[0]).toHaveProperty('id')
        expect(todosFromDB[0]).toHaveProperty('todoName')
    })


    it('addTodo should add the new todo to the database', async () => {
        let uniqueStringName = uuidv4()
        const response = await addTodo({ todoName: uniqueStringName })

        expect(response.todoName).toBe(uniqueStringName)
        expect(response).toHaveProperty('id')

        const todos = await Todo.find().select({ "todoName": 1, "id": 1, "_id": 0 })
        expect(todos.map(todo => todo.todoName).includes(uniqueStringName)).toBe(true)
        expect(todos.map(todo => todo.id).includes(response.id)).toBe(true)
    })

})
