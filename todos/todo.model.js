const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    todoName: {
        type: String
    },
    id: {
        type: String,
        unique: true
    }
})

const Todo = mongoose.model('todo', TodoSchema)
module.exports = Todo