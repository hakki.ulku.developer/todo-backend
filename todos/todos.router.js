const express = require("express")
const router = express.Router()
const { addTodo, getAllTodo } = require('./todos.services')

/** Returns an array of todos */
router.get('/todos', async (req, res, next) => {
    const todos = await getAllTodo()
    res.send(todos)
})

/*
    * Recevies a new todo and saves it to the MongoDb then returns the newly created todo 
    * If the incoming body missing 'todoName' field, then return 400 (Bad Request)  response
*/
router.post('/addTodo', async (req, res, next) => {
    const newTodo = req.body
    const response = await addTodo(newTodo);

    if (response.message) {
        res.status(400).send({ message: response.message })
        return
    }
    res.status(201).send({ id: response.id, todoName: response.todoName })
})

module.exports = router