// here I will have database related functions etc..

const Todo = require('./todo.model')
const { v4: uuidv4 } = require('uuid');

/**
 * Returns all todos from the database
 *
 * @return {*} 
 */
async function getAllTodo() {
    return Todo.find().select({ "todoName": 1, "id": 1, "_id": 0 })
}


/**
 * Creates a new todo in the database
 *
 * @param {*} newTodo
 * @return {*} 
 */
async function addTodo(newTodo) {
    if (typeof newTodo !== 'object') {
        return { message: "request body must contain todoName, but todoName is missing!" }
    }

    if (!Object.keys(newTodo).includes("todoName")) {
        return { message: "request body must contain todoName, but todoName is missing!" }
    }

    const todo = new Todo({ todoName: newTodo.todoName, id: uuidv4() })
    return todo.save()
}

module.exports = {
    getAllTodo,
    addTodo
}